/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pooii.your_bookshelf.repositorios;

import pooii.your_bookshelf.modelos.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author elias
 */
public class Your_BookshelfRepositorio {

    private final Connection conexion;

    public Your_BookshelfRepositorio(Connection connection) throws SQLException{
        this.conexion = connection;
        var consulta = connection.createStatement();
        consulta.execute("CREATE TABLE IF NOT EXISTS your_bookshelf (identificador INTEGER PRIMARY KEY AUTOINCREMENT, nombre TEXT)");
        consulta.close();
    }
    
    //------------------------
    // AUTOR
    //------------------------

    public Autor obtenerAutor(int identificador)
            throws SQLException, AutorNoEncontradoExcepcion {
        var consulta = conexion.prepareStatement("SELECT id, name, email FROM Autor WHERE id = ?");
        consulta.setInt(1, identificador);
        var resultado = consulta.executeQuery();
        try {
            if (resultado.next()) {
                return new Autor(
                        resultado.getString("id"),
                        resultado.getString("Nombres"),
                        resultado.getString("Email"));
            } else {
                throw new AutorNoEncontradoExcepcion();
            }
        } finally {
            consulta.close();
            resultado.close();
        }
    }
    
    //------------------------
    // LIBRO
    //------------------------

    public Libro obtenerLibro(String nombre)
            throws SQLException, LibroNoEncontradoExcepcion {
        var consulta = conexion.prepareStatement("SELECT id FROM Libro WHERE nombre = ?");
        consulta.setString(0,"nombre");
        var resultado = consulta.executeQuery();
        try {
            if (resultado.next()) {
                return new Libro(
                        resultado.getString("Nombre"));
            } else {
                throw new LibroNoEncontradoExcepcion();
            }
        } finally {
            consulta.close();
            resultado.close();
        }
    }
    
    public void agregarLibro(String nombre) throws SQLException{
        var consulta = conexion.prepareStatement("INSERT INTO Your_Bookshelf (nombre) VALUES (?)");
        consulta.setString(1, nombre);
        consulta.executeUpdate();
        consulta.close();
    }
    
    public void modificarLibro(Libro libro) throws SQLException, LibroNoEncontradoExcepcion{
        var consulta = conexion.prepareStatement("UPDATE libro SET nombre = ?");
        consulta.setString(1,libro.getId());//to recap
        try{
            if (consulta.executeUpdate() == 0) throw new LibroNoEncontradoExcepcion();
        }
        finally{
            consulta.close();
        }
    }

    public List<Libro> listar() throws SQLException {
        var libros = new ArrayList<Libro>();
        var consulta = conexion.prepareStatement("");
        var resultado = consulta.executeQuery();
        while (resultado.next()) {

        }
        resultado.close();
        consulta.close();
        return libros;
    }

    //------------------------
    // Categoria
    //------------------------
    public Categoria obtenerCategoria(String id, String name)
            throws SQLException, CategoriaNoEncontradaExcepcion {
        var consulta = conexion.prepareStatement("SELECT id,nombre FROM Cateogoria WEHRE id = ?");
        consulta.setString(1, "name");
        var resultado = consulta.executeQuery();
        try {
            if (resultado.next()) {
               return new Categoria (
                            resultado.getString(1),
                            resultado.getString("name"));
            }
            else{
                throw new CategoriaNoEncontradaExcepcion();
            }
        } finally {
            consulta.close();
            resultado.close();
        }

    }

    //------------------------
    // EDITORIAL
    //------------------------
    public Editor obtenerEditor(String name, Libro libro)
            throws SQLException, EditorNoEncontradoExcepcion {
        var consulta = conexion.prepareStatement("SELECT name FROM Editor");
        
        var resultado = consulta.executeQuery();
        try {
            if (resultado.next()) {
                return new Editor(resultado.getString("Nombre"),resultado.getObject("libro",Libro.class));
            }
            else{
                throw new EditorNoEncontradoExcepcion();
            }   
        } finally {
            consulta.close();
            resultado.close();
        }
    }
    
    //------------------------
    // VALORIZACION
    //------------------------
    
    public Valorizacion obtenerValorizacion(String puntos,String comment,Libro libro) throws SQLException, ValorizacionNoEncontradaExcepcion{
        var consulta = conexion.prepareStatement("SELECT puntos,comment FROM Valorizacion");
        
        var resultado = consulta.executeQuery();
        try{
            if (resultado.next()){
                return new Valorizacion(resultado.getString("puntos"),resultado.getString("comment"),resultado.getObject("Libro", Libro.class));
            }
            else{
                throw new ValorizacionNoEncontradaExcepcion();
            }
        }finally{
            consulta.close();
            resultado.close();
        }
    }
}
