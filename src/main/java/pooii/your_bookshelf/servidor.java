/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pooii.your_bookshelf;

import io.javalin.Javalin;
import java.sql.DriverManager;
import java.sql.SQLException;
import pooii.your_bookshelf.repositorios.Your_BookshelfRepositorio;
import pooii.your_bookshelf.repositorios.LibroNoEncontradoExcepcion;
import pooii.your_bookshelf.controladores.Your_BookshelfControladores;
import static io.javalin.apibuilder.ApiBuilder.*;
import io.javalin.core.event.EventListener;

public class servidor {
    public static void main(String[] args) throws SQLException {
        var conexion = DriverManager.getConnection("jdbc:sqlite:ur_bookshelf.db");
        var Your_BookshelfRepositorio = new Your_BookshelfRepositorio(conexion);
        var Your_BookshelfControlador = new Your_BookshelfControladores(Your_BookshelfRepositorio);
        
        Javalin.create()
        .events((EventListener event) -> {
            event.serverStopped(() -> { conexion.close(); });
        })
        .routes(() -> {
            path("Your_Bookshelf", () -> {
                get(Your_BookshelfControlador::listar);
                path(":identificador", () -> {
                    delete(null);
                    put(null);
                });
            });    
        })
        .exception(LibroNoEncontradoExcepcion.class,(e, ctx) -> {ctx.status(404);})
        .start(7000);
    }
}
