/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pooii.your_bookshelf.modelos;

/**
 *
 * @author elias
 */
public class Categoria {

  //------------------------
  // MEMBER VARIABLES
  //------------------------

  //Categoria Attributes
  private String id;
  private String name;

  //Categoria Associations
  private Libro libro;

  //------------------------
  // CONSTRUCTOR
  //------------------------

  public Categoria(String aId, String aName)
  {
    id = aId;
    name = aName;
    /*if (aLibro == null || aLibro.getCategoria() != null)
    {
      throw new RuntimeException("Unable to create Categoria due to aLibro. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
    }
    libro = aLibro;*/
  }

  public Categoria(String aId, String aName, String aIdForLibro, Valorizacion aValorizacionForLibro, Editor aEditorForLibro)
  {
    id = aId;
    name = aName;
    libro = new Libro(aIdForLibro, this, aValorizacionForLibro, aEditorForLibro);
  }

  //------------------------
  // INTERFACE
  //------------------------

  public boolean setId(String aId)
  {
    boolean wasSet = false;
    id = aId;
    wasSet = true;
    return wasSet;
  }

  public boolean setName(String aName)
  {
    boolean wasSet = false;
    name = aName;
    wasSet = true;
    return wasSet;
  }

  public String getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }
  /* Code from template association_GetOne */
  public Libro getLibro()
  {
    return libro;
  }

  public void delete()
  {
    Libro existingLibro = libro;
    libro = null;
    if (existingLibro != null)
    {
      existingLibro.delete();
    }
  }


  public String toString()
  {
    return super.toString() + "["+
            "id" + ":" + getId()+ "," +
            "name" + ":" + getName()+ "]" + System.getProperties().getProperty("line.separator") +
            "  " + "libro = "+(getLibro()!=null?Integer.toHexString(System.identityHashCode(getLibro())):"null");
  }    
}
