/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pooii.your_bookshelf.modelos;

/**
 *
 * @author elias
 */
public class Valorizacion {

  //------------------------
  // MEMBER VARIABLES
  //------------------------

  //Valorizacion Attributes
  private String puntos;
  private String comment;

  //Valorizacion Associations
  private Libro libro;

  //------------------------
  // CONSTRUCTOR
  //------------------------

  public Valorizacion(String aPuntos, String aComment, Libro aLibro)
  {
    puntos = aPuntos;
    comment = aComment;
    if (aLibro == null || aLibro.getValorizacion() != null)
    {
      throw new RuntimeException("Unable to create Valorizacion due to aLibro. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
    }
    libro = aLibro;
  }

  public Valorizacion(String aPuntos, String aComment, String aIdForLibro, Categoria aCategoriaForLibro, Editor aEditorForLibro)
  {
    puntos = aPuntos;
    comment = aComment;
    libro = new Libro(aIdForLibro, aCategoriaForLibro, this, aEditorForLibro);
  }

  //------------------------
  // INTERFACE
  //------------------------

  public boolean setPuntos(String aPuntos)
  {
    boolean wasSet = false;
    puntos = aPuntos;
    wasSet = true;
    return wasSet;
  }

  public boolean setComment(String aComment)
  {
    boolean wasSet = false;
    comment = aComment;
    wasSet = true;
    return wasSet;
  }

  public String getPuntos()
  {
    return puntos;
  }

  public String getComment()
  {
    return comment;
  }
  /* Code from template association_GetOne */
  public Libro getLibro()
  {
    return libro;
  }

  public void delete()
  {
    Libro existingLibro = libro;
    libro = null;
    if (existingLibro != null)
    {
      existingLibro.delete();
    }
  }


  public String toString()
  {
    return super.toString() + "["+
            "puntos" + ":" + getPuntos()+ "," +
            "comment" + ":" + getComment()+ "]" + System.getProperties().getProperty("line.separator") +
            "  " + "libro = "+(getLibro()!=null?Integer.toHexString(System.identityHashCode(getLibro())):"null");
  }    
}
