/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pooii.your_bookshelf.modelos;

/**
 *
 * @author elias
 */
public class Editor {

  //------------------------
  // MEMBER VARIABLES
  //------------------------

  //Editor Attributes
  private String name;
  private String address;
  private String tel;

  //Editor Associations
  private Libro libro;

  //------------------------
  // CONSTRUCTOR
  //------------------------

  public Editor(String aName, Libro aLibro)
  {
    name = aName;
    if (aLibro == null || aLibro.getEditor() != null)
    {
      throw new RuntimeException("Unable to create Editor due to aLibro. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
    }
    libro = aLibro;
  }

  public Editor(String aName, String aAddress, String aTel, String aIdForLibro, Categoria aCategoriaForLibro, Valorizacion aValorizacionForLibro)
  {
    name = aName;
    address = aAddress;
    tel = aTel;
    libro = new Libro(aIdForLibro, aCategoriaForLibro, aValorizacionForLibro, this);
  }

  //------------------------
  // INTERFACE
  //------------------------

  public boolean setName(String aName)
  {
    boolean wasSet = false;
    name = aName;
    wasSet = true;
    return wasSet;
  }

  public boolean setAddress(String aAddress)
  {
    boolean wasSet = false;
    address = aAddress;
    wasSet = true;
    return wasSet;
  }

  public boolean setTel(String aTel)
  {
    boolean wasSet = false;
    tel = aTel;
    wasSet = true;
    return wasSet;
  }

  public String getName()
  {
    return name;
  }

  public String getAddress()
  {
    return address;
  }

  public String getTel()
  {
    return tel;
  }
  /* Code from template association_GetOne */
  public Libro getLibro()
  {
    return libro;
  }

  public void delete()
  {
    Libro existingLibro = libro;
    libro = null;
    if (existingLibro != null)
    {
      existingLibro.delete();
    }
  }


  public String toString()
  {
    return super.toString() + "["+
            "name" + ":" + getName()+ "," +
            "address" + ":" + getAddress()+ "," +
            "tel" + ":" + getTel()+ "]" + System.getProperties().getProperty("line.separator") +
            "  " + "libro = "+(getLibro()!=null?Integer.toHexString(System.identityHashCode(getLibro())):"null");
  }    
}
