/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pooii.your_bookshelf.modelos;

/**
 *
 * @author elias
 */
public class Edicion_del_Libro {

  //------------------------
  // MEMBER VARIABLES
  //------------------------

  //Edicion_del_Libro Attributes
  private String idioma;
  private String licencia;
  private String publicacion;
  private String isbn;
  private String paginas;
  private String descripcion;

  //Edicion_del_Libro Associations
  private Libro libro;

  //------------------------
  // CONSTRUCTOR
  //------------------------

  public Edicion_del_Libro(String aIdioma, String aLicencia, String aPublicacion, String aIsbn, String aPaginas, String aDescripcion, Libro aLibro)
  {
    idioma = aIdioma;
    licencia = aLicencia;
    publicacion = aPublicacion;
    isbn = aIsbn;
    paginas = aPaginas;
    descripcion = aDescripcion;
    boolean didAddLibro = setLibro(aLibro);
    if (!didAddLibro)
    {
      throw new RuntimeException("Unable to create edicionDelLibro due to libro. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
    }
  }

  //------------------------
  // INTERFACE
  //------------------------

  public boolean setIdioma(String aIdioma)
  {
    boolean wasSet = false;
    idioma = aIdioma;
    wasSet = true;
    return wasSet;
  }

  public boolean setLicencia(String aLicencia)
  {
    boolean wasSet = false;
    licencia = aLicencia;
    wasSet = true;
    return wasSet;
  }

  public boolean setPublicacion(String aPublicacion)
  {
    boolean wasSet = false;
    publicacion = aPublicacion;
    wasSet = true;
    return wasSet;
  }

  public boolean setIsbn(String aIsbn)
  {
    boolean wasSet = false;
    isbn = aIsbn;
    wasSet = true;
    return wasSet;
  }

  public boolean setPaginas(String aPaginas)
  {
    boolean wasSet = false;
    paginas = aPaginas;
    wasSet = true;
    return wasSet;
  }

  public boolean setDescripcion(String aDescripcion)
  {
    boolean wasSet = false;
    descripcion = aDescripcion;
    wasSet = true;
    return wasSet;
  }

  public String getIdioma()
  {
    return idioma;
  }

  public String getLicencia()
  {
    return licencia;
  }

  public String getPublicacion()
  {
    return publicacion;
  }

  public String getIsbn()
  {
    return isbn;
  }

  public String getPaginas()
  {
    return paginas;
  }

  public String getDescripcion()
  {
    return descripcion;
  }
  /* Code from template association_GetOne */
  public Libro getLibro()
  {
    return libro;
  }
  /* Code from template association_SetOneToMany */
  public boolean setLibro(Libro aLibro)
  {
    boolean wasSet = false;
    if (aLibro == null)
    {
      return wasSet;
    }

    Libro existingLibro = libro;
    libro = aLibro;
    if (existingLibro != null && !existingLibro.equals(aLibro))
    {
      existingLibro.removeEdicionDelLibro(this);
    }
    libro.addEdicionDelLibro(this);
    wasSet = true;
    return wasSet;
  }

  public void delete()
  {
    Libro placeholderLibro = libro;
    this.libro = null;
    if(placeholderLibro != null)
    {
      placeholderLibro.removeEdicionDelLibro(this);
    }
  }


  public String toString()
  {
    return super.toString() + "["+
            "idioma" + ":" + getIdioma()+ "," +
            "licencia" + ":" + getLicencia()+ "," +
            "publicacion" + ":" + getPublicacion()+ "," +
            "isbn" + ":" + getIsbn()+ "," +
            "paginas" + ":" + getPaginas()+ "," +
            "descripcion" + ":" + getDescripcion()+ "]" + System.getProperties().getProperty("line.separator") +
            "  " + "libro = "+(getLibro()!=null?Integer.toHexString(System.identityHashCode(getLibro())):"null");
  }   
}
