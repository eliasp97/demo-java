/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pooii.your_bookshelf.modelos;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/**
 *
 * @author elias
 */
public class Libro {

  //------------------------
  // MEMBER VARIABLES
  //------------------------

  //Libro Attributes
  private String id;

  //Libro Associations
  private Categoria categoria;
  private Valorizacion valorizacion;
  private Editor editor;
  private List<Edicion_del_Libro> edicionDelLibros;
  private List<Autor> autors;

  //------------------------
  // CONSTRUCTOR
  //------------------------

  public Libro(String id)
  {
        this.id = id;
  }

    public Libro(String aId, Categoria aCategoria, Valorizacion aValorizacion, Editor aEditor) {
        id = aId;
        if (aCategoria == null || aCategoria.getLibro() != null)
        {
            throw new RuntimeException("Unable to create Libro due to aCategoria. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
        }
        categoria = aCategoria;
        if (aValorizacion == null || aValorizacion.getLibro() != null)
        {
            throw new RuntimeException("Unable to create Libro due to aValorizacion. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
        }
        valorizacion = aValorizacion;
        if (aEditor == null || aEditor.getLibro() != null)
        {
            throw new RuntimeException("Unable to create Libro due to aEditor. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
        }
        editor = aEditor;
        edicionDelLibros = new ArrayList<Edicion_del_Libro>();
        autors = new ArrayList<Autor>();
    }

  public Libro(String aId, String aNameForCategoria, String aPuntosForValorizacion, String aCommentForValorizacion, String aNameForEditor, String aAddressForEditor, String aTelForEditor)
  {
    id = aId;
    valorizacion = new Valorizacion(aPuntosForValorizacion, aCommentForValorizacion, this);
    editor = new Editor(aNameForEditor, this);
    edicionDelLibros = new ArrayList<Edicion_del_Libro>();
    autors = new ArrayList<Autor>();
  }

  //------------------------
  // INTERFACE
  //------------------------

  public boolean setId(String aId)
  {
    boolean wasSet = false;
    id = aId;
    wasSet = true;
    return wasSet;
  }

  public String getId()
  {
    return id;
  }
  /* Code from template association_GetOne */
  public Categoria getCategoria()
  {
    return categoria;
  }
  /* Code from template association_GetOne */
  public Valorizacion getValorizacion()
  {
    return valorizacion;
  }
  /* Code from template association_GetOne */
  public Editor getEditor()
  {
    return editor;
  }
  /* Code from template association_GetMany */
  public Edicion_del_Libro getEdicionDelLibro(int index)
  {
    Edicion_del_Libro aEdicionDelLibro = edicionDelLibros.get(index);
    return aEdicionDelLibro;
  }

  public List<Edicion_del_Libro> getEdicionDelLibros()
  {
    List<Edicion_del_Libro> newEdicionDelLibros = Collections.unmodifiableList(edicionDelLibros);
    return newEdicionDelLibros;
  }

  public int numberOfEdicionDelLibros()
  {
    int number = edicionDelLibros.size();
    return number;
  }

  public boolean hasEdicionDelLibros()
  {
    boolean has = edicionDelLibros.size() > 0;
    return has;
  }

  public int indexOfEdicionDelLibro(Edicion_del_Libro aEdicionDelLibro)
  {
    int index = edicionDelLibros.indexOf(aEdicionDelLibro);
    return index;
  }
  /* Code from template association_GetMany */
  public Autor getAutor(int index)
  {
    Autor aAutor = autors.get(index);
    return aAutor;
  }

  public List<Autor> getAutors()
  {
    List<Autor> newAutors = Collections.unmodifiableList(autors);
    return newAutors;
  }

  public int numberOfAutors()
  {
    int number = autors.size();
    return number;
  }

  public boolean hasAutors()
  {
    boolean has = autors.size() > 0;
    return has;
  }

  public int indexOfAutor(Autor aAutor)
  {
    int index = autors.indexOf(aAutor);
    return index;
  }
  /* Code from template association_MinimumNumberOfMethod */
  public static int minimumNumberOfEdicionDelLibros()
  {
    return 0;
  }
  /* Code from template association_AddManyToOne */
  public Edicion_del_Libro addEdicionDelLibro(String aIdioma, String aLicencia, String aPublicacion, String aIsbn, String aPaginas, String aDescripcion)
  {
    return new Edicion_del_Libro(aIdioma, aLicencia, aPublicacion, aIsbn, aPaginas, aDescripcion, this);
  }

  public boolean addEdicionDelLibro(Edicion_del_Libro aEdicionDelLibro)
  {
    boolean wasAdded = false;
    if (edicionDelLibros.contains(aEdicionDelLibro)) { return false; }
    Libro existingLibro = aEdicionDelLibro.getLibro();
    boolean isNewLibro = existingLibro != null && !this.equals(existingLibro);
    if (isNewLibro)
    {
      aEdicionDelLibro.setLibro(this);
    }
    else
    {
      edicionDelLibros.add(aEdicionDelLibro);
    }
    wasAdded = true;
    return wasAdded;
  }

  public boolean removeEdicionDelLibro(Edicion_del_Libro aEdicionDelLibro)
  {
    boolean wasRemoved = false;
    //Unable to remove aEdicionDelLibro, as it must always have a libro
    if (!this.equals(aEdicionDelLibro.getLibro()))
    {
      edicionDelLibros.remove(aEdicionDelLibro);
      wasRemoved = true;
    }
    return wasRemoved;
  }
  /* Code from template association_AddIndexControlFunctions */
  public boolean addEdicionDelLibroAt(Edicion_del_Libro aEdicionDelLibro, int index)
  {  
    boolean wasAdded = false;
    if(addEdicionDelLibro(aEdicionDelLibro))
    {
      if(index < 0 ) { index = 0; }
      if(index > numberOfEdicionDelLibros()) { index = numberOfEdicionDelLibros() - 1; }
      edicionDelLibros.remove(aEdicionDelLibro);
      edicionDelLibros.add(index, aEdicionDelLibro);
      wasAdded = true;
    }
    return wasAdded;
  }

  public boolean addOrMoveEdicionDelLibroAt(Edicion_del_Libro aEdicionDelLibro, int index)
  {
    boolean wasAdded = false;
    if(edicionDelLibros.contains(aEdicionDelLibro))
    {
      if(index < 0 ) { index = 0; }
      if(index > numberOfEdicionDelLibros()) { index = numberOfEdicionDelLibros() - 1; }
      edicionDelLibros.remove(aEdicionDelLibro);
      edicionDelLibros.add(index, aEdicionDelLibro);
      wasAdded = true;
    } 
    else 
    {
      wasAdded = addEdicionDelLibroAt(aEdicionDelLibro, index);
    }
    return wasAdded;
  }
  /* Code from template association_MinimumNumberOfMethod */
  public static int minimumNumberOfAutors()
  {
    return 0;
  }
  /* Code from template association_AddManyToOne */
  public Autor addAutor(String aId, String aName, String aEmail, String aAddress, String aTel)
  {
    return new Autor(aId, aName, aEmail, aAddress, aTel, this);
  }

  public boolean addAutor(Autor aAutor)
  {
    boolean wasAdded = false;
    if (autors.contains(aAutor)) { return false; }
    Libro existingLibro = aAutor.getLibro();
    boolean isNewLibro = existingLibro != null && !this.equals(existingLibro);
    if (isNewLibro)
    {
      aAutor.setLibro(this);
    }
    else
    {
      autors.add(aAutor);
    }
    wasAdded = true;
    return wasAdded;
  }

  public boolean removeAutor(Autor aAutor)
  {
    boolean wasRemoved = false;
    //Unable to remove aAutor, as it must always have a libro
    if (!this.equals(aAutor.getLibro()))
    {
      autors.remove(aAutor);
      wasRemoved = true;
    }
    return wasRemoved;
  }
  /* Code from template association_AddIndexControlFunctions */
  public boolean addAutorAt(Autor aAutor, int index)
  {  
    boolean wasAdded = false;
    if(addAutor(aAutor))
    {
      if(index < 0 ) { index = 0; }
      if(index > numberOfAutors()) { index = numberOfAutors() - 1; }
      autors.remove(aAutor);
      autors.add(index, aAutor);
      wasAdded = true;
    }
    return wasAdded;
  }

  public boolean addOrMoveAutorAt(Autor aAutor, int index)
  {
    boolean wasAdded = false;
    if(autors.contains(aAutor))
    {
      if(index < 0 ) { index = 0; }
      if(index > numberOfAutors()) { index = numberOfAutors() - 1; }
      autors.remove(aAutor);
      autors.add(index, aAutor);
      wasAdded = true;
    } 
    else 
    {
      wasAdded = addAutorAt(aAutor, index);
    }
    return wasAdded;
  }

  public void delete()
  {
    Categoria existingCategoria = categoria;
    categoria = null;
    if (existingCategoria != null)
    {
      existingCategoria.delete();
    }
    Valorizacion existingValorizacion = valorizacion;
    valorizacion = null;
    if (existingValorizacion != null)
    {
      existingValorizacion.delete();
    }
    Editor existingEditor = editor;
    editor = null;
    if (existingEditor != null)
    {
      existingEditor.delete();
    }
    for(int i=edicionDelLibros.size(); i > 0; i--)
    {
      Edicion_del_Libro aEdicionDelLibro = edicionDelLibros.get(i - 1);
      aEdicionDelLibro.delete();
    }
    for(int i=autors.size(); i > 0; i--)
    {
      Autor aAutor = autors.get(i - 1);
      aAutor.delete();
    }
  }


  public String toString()
  {
    return super.toString() + "["+
            "id" + ":" + getId()+ "]" + System.getProperties().getProperty("line.separator") +
            "  " + "categoria = "+(getCategoria()!=null?Integer.toHexString(System.identityHashCode(getCategoria())):"null") + System.getProperties().getProperty("line.separator") +
            "  " + "valorizacion = "+(getValorizacion()!=null?Integer.toHexString(System.identityHashCode(getValorizacion())):"null") + System.getProperties().getProperty("line.separator") +
            "  " + "editor = "+(getEditor()!=null?Integer.toHexString(System.identityHashCode(getEditor())):"null");
  }    
}
