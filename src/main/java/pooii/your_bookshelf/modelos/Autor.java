/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pooii.your_bookshelf.modelos;

/**
 *
 * @author elias
 */
public class Autor {

  //------------------------
  // MEMBER VARIABLES
  //------------------------

  //Autor Attributes
  private String id;
  private String name;
  private String email;
  private String address;
  private String tel;

  //Autor Associations
  private Libro libro;

  //------------------------
  // CONSTRUCTOR
  //------------------------

  public Autor(String id, String name, String email)
  {
        this.id = id;
        this.name = name;
        this.email = email;
  }

    public Autor(String aId, String aName, String aEmail, String aAddress, String aTel, Libro aLibro) {
        id = aId;
        name = aName;
        email = aEmail;
        address = aAddress;
        tel = aTel;
        boolean didAddLibro = setLibro(aLibro);
        if (!didAddLibro)
        {
            throw new RuntimeException("Unable to create autor due to libro. See http://manual.umple.org?RE002ViolationofAssociationMultiplicity.html");
        }
    }

  //------------------------
  // INTERFACE
  //------------------------

  public boolean setId(String aId)
  {
    boolean wasSet = false;
    id = aId;
    wasSet = true;
    return wasSet;
  }

  public boolean setName(String aName)
  {
    boolean wasSet = false;
    name = aName;
    wasSet = true;
    return wasSet;
  }

  public boolean setEmail(String aEmail)
  {
    boolean wasSet = false;
    email = aEmail;
    wasSet = true;
    return wasSet;
  }

  public boolean setAddress(String aAddress)
  {
    boolean wasSet = false;
    address = aAddress;
    wasSet = true;
    return wasSet;
  }

  public boolean setTel(String aTel)
  {
    boolean wasSet = false;
    tel = aTel;
    wasSet = true;
    return wasSet;
  }

  public String getId()
  {
    return id;
  }

  public String getName()
  {
    return name;
  }

  public String getEmail()
  {
    return email;
  }

  public String getAddress()
  {
    return address;
  }

  public String getTel()
  {
    return tel;
  }
  /* Code from template association_GetOne */
  public Libro getLibro()
  {
    return libro;
  }
  /* Code from template association_SetOneToMany */
  public boolean setLibro(Libro aLibro)
  {
    boolean wasSet = false;
    if (aLibro == null)
    {
      return wasSet;
    }

    Libro existingLibro = libro;
    libro = aLibro;
    if (existingLibro != null && !existingLibro.equals(aLibro))
    {
      existingLibro.removeAutor(this);
    }
    libro.addAutor(this);
    wasSet = true;
    return wasSet;
  }

  public void delete()
  {
    Libro placeholderLibro = libro;
    this.libro = null;
    if(placeholderLibro != null)
    {
      placeholderLibro.removeAutor(this);
    }
  }


  public String toString()
  {
    return super.toString() + "["+
            "id" + ":" + getId()+ "," +
            "name" + ":" + getName()+ "," +
            "email" + ":" + getEmail()+ "," +
            "address" + ":" + getAddress()+ "," +
            "tel" + ":" + getTel()+ "]" + System.getProperties().getProperty("line.separator") +
            "  " + "libro = "+(getLibro()!=null?Integer.toHexString(System.identityHashCode(getLibro())):"null");
  }    
}
