/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pooii.your_bookshelf.controladores;

import pooii.your_bookshelf.modelos.*;
import pooii.your_bookshelf.repositorios.Your_BookshelfRepositorio;
import pooii.your_bookshelf.repositorios.LibroNoEncontradoExcepcion;
import io.javalin.http.Context;
import java.sql.SQLException;
/**
 *
 * @author elias
 */
public class Your_BookshelfControladores {
    private final Your_BookshelfRepositorio ur_bookshelfRepositorio;

    public Your_BookshelfControladores(Your_BookshelfRepositorio ur_bookshelfRepositorio) {
        this.ur_bookshelfRepositorio = ur_bookshelfRepositorio;
    }
    
    public void listar(Context ctx) throws SQLException{
        ctx.json(ur_bookshelfRepositorio.listar());
    }
    
    public void crear (Context ctx) throws SQLException{
        ur_bookshelfRepositorio.agregarLibro(ctx.formParam("nombre",String.class).get());
    }
    
    public void modificar(Context ctx) throws SQLException, LibroNoEncontradoExcepcion {
        var libro = ur_bookshelfRepositorio.obtenerLibro(ctx.pathParam("nombre",String.class).get());
        ur_bookshelfRepositorio.modificarLibro(libro);
        ctx.status(204);
    }
}
